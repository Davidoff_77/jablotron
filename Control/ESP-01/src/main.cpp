#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <AsyncElegantOTA.h>
#include <SoftwareSerial.h>

//3 seconds WDT
#define WDT_TIMEOUT 15

const char *SSID = "MDK2.4";
const char *PWD = "PassMDK#77";

AsyncWebServer server(80);
SoftwareSerial SerialPort;  // RX, TX
const int baudRate = 9600;
//HardwareSerial SerialPort(0);  //if using UART1

const char* PIN_INPUT = "pin";
const char* COMMAND_INPUT = "command";
const char* PARAM_INPUT = "param";
const char* EXTRA_INPUT = "extra";
const char* HOSTNAME = "ESP32 Jablotron";

//WDT
int last = millis();
int i = 0;

void setup_routing() {     
  // Send web page with input fields to client
  // Send a GET request to <ESP_IP>/get?input1=<inputMessage>
  server.on("/get", HTTP_GET, [] (AsyncWebServerRequest *request) {
    String inputMessage;
    if (request->hasParam(COMMAND_INPUT)) 
    {       
      Serial.println("COMMAND: "+request->getParam(COMMAND_INPUT)->value());
      if(request->hasParam(PIN_INPUT))
      {
        Serial.println("PIN_INPUT: "+request->getParam(PIN_INPUT)->value());
        inputMessage = request->getParam(PIN_INPUT)->value() + " " + request->getParam(COMMAND_INPUT)->value();
        if(request->hasParam(PARAM_INPUT))
        {
          Serial.println("PARAM_INPUT: "+request->getParam(PARAM_INPUT)->value());
          inputMessage += " " + request->getParam(PARAM_INPUT)->value();
        }
      }

      SerialPort.print(inputMessage);
      SerialPort.print("\r\n"); 
    
      Serial.println(inputMessage);

      String stringReturn = "";
      // set timeout for UART response
      unsigned long start_time = millis();
      while(SerialPort.available() == 0 && millis() - start_time < 2000){
        //Serial.println("Waiting for data...");
        //yield();
        
        delay(50);  //změna yield() na delay()
      }

      if (SerialPort.available() > 0) {
        stringReturn = SerialPort.readString();
        request->send(200, "text/html", "je to cajk");
      }
      else
      {
        request->send(500, "text/html", "divny");
      }

      request->send(200, "text/html", stringReturn);
    } 
    else 
    {
        // send error response if semaphore can not be acquired
        request->send(503, "text/html", "Semaphore not available");
    }
  });

  server.on("/healthcheck", HTTP_GET, [] (AsyncWebServerRequest *request) {
    Serial.println("Healthcheck");
    request->send(200, "text/html", system_get_sdk_version());
  }); 
  server.begin();    
}

void setup() {     
  Serial.begin(baudRate, SERIAL_8N1, SERIAL_TX_ONLY,);
  SerialPort.begin(baudRate, SWSERIAL_8N1, 0, 2);

  WiFi.setHostname(HOSTNAME);
  WiFi.begin(SSID, PWD);
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    delay(5000);
    ESP.restart();
  }
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
  }
 
  AsyncElegantOTA.begin(&server); 

  setup_routing();     
   
}    
       
void loop() {     
}