#include <Arduino.h>
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <AsyncElegantOTA.h>

#define WDT_TIMEOUT 15

const char* SSID = "MDK2.4";
const char* PWD = "PassMDK#77";

AsyncWebServer server(80);
HardwareSerial SerialPort(1);
SemaphoreHandle_t xSemaphore = NULL;

const char* PIN_INPUT = "pin";
const char* COMMAND_INPUT = "command";
const char* PARAM_INPUT = "param";
const char* HOSTNAME = "ESP32 Jablotron";

int lastWDTReset = 0;

// Funkce pro blokující čtení ze sériového portu s časovým omezením
String readSerialWithTimeout(unsigned long timeout) {
  String response = "";
  unsigned long startTime = millis();
  
  while (SerialPort.available() == 0 && millis() - startTime < timeout) {
    yield();
  }
  
  if (SerialPort.available() > 0) {
    response = SerialPort.readString();
  }
  
  return response;
}

void handleRequest(AsyncWebServerRequest* request) {
  String inputMessage;
  
  if (request->hasParam(COMMAND_INPUT)) {
    if (xSemaphoreTake(xSemaphore, pdMS_TO_TICKS(10000)) == pdTRUE) {
      if (request->hasParam(PIN_INPUT)) {
        inputMessage = request->getParam(PIN_INPUT)->value() + " " + request->getParam(COMMAND_INPUT)->value();
        
        if (request->hasParam(PARAM_INPUT)) {
          inputMessage += " " + request->getParam(PARAM_INPUT)->value();
        }
      }
      
      SerialPort.print(inputMessage);
      SerialPort.print("\r\n");
      Serial.print(inputMessage);

      String stringReturn = readSerialWithTimeout(2000);

      xSemaphoreGive(xSemaphore);

      request->send(200, "text/html", stringReturn);
    } else {
      request->send(503, "text/html", "Semaphore not available");
    }
  } else {
    request->send(400, "text/html", "error");
  }
}

void handleHealthCheck(AsyncWebServerRequest* request) {
  request->send(200, "text/html", esp_get_idf_version());
}

void setupWiFi() {
  WiFi.setHostname(HOSTNAME);
  WiFi.begin(SSID, PWD);

  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed! Rebooting...");
    delay(5000);
    ESP.restart();
  }

  Serial.print("Connected! IP Address: ");
  Serial.println(WiFi.localIP());
}

void setupServer() {
  server.on("/get", HTTP_GET, handleRequest);
  server.on("/healthcheck", HTTP_GET, handleHealthCheck);
  server.begin();
}

void setup() {
  Serial.begin(9600);
  SerialPort.begin(9600, SERIAL_8N1, 16, 17);

  esp_task_wdt_init(WDT_TIMEOUT, true);
  esp_task_wdt_add(NULL);

  xSemaphore = xSemaphoreCreateBinary();
  xSemaphoreGive(xSemaphore);

  setupWiFi();
  setupServer();

  AsyncElegantOTA.begin(&server);
}

void loop() {
  // Reset watchdog timer
  if (millis() - lastWDTReset >= 10000) {
    esp_task_wdt_reset();
    lastWDTReset = millis();
  }

  AsyncElegantOTA.loop();
}