#include "esphome.h"
#include <vector>
#include <sstream>
#include <bitset>

using namespace std;

//please read notes in .yaml file

class JA121T : public Component, public UARTDevice {
 public:
  JA121T(UARTComponent *parent) : UARTDevice(parent) {}
  
  void setup() override {
    // This will be called by App.setup()
  }

  //Sensors
  TextSensor *received_line = new TextSensor();
  TextSensor *version = new TextSensor();
  TextSensor *error = new TextSensor();  
  //PG[0] is not used
  TextSensor * PG[11] = { new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(),
    new TextSensor() };
  //STATE[0] is not used
  TextSensor * STATE[11] = { new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(),
    new TextSensor() };
  //PRFSTATE[0] is used
  /*  
  TextSensor * PRFSTATE[64] = { new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(),
  new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(),
  new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(),
  new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(),
  new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(),
  new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor(),  
  new TextSensor(), new TextSensor(), new TextSensor(), new TextSensor() };  
  */

  vector<string> split (const string &s, char delim) {
    vector<string> result;
    stringstream ss (s);
    string item;
    while (getline (ss, item, delim)) {
        result.push_back (item);
    }
    return result;
  }

  int readline(int readch, char *buffer, int len)
  {
    static int pos = 0;
    int rpos;

    if (readch > 0) {
      switch (readch) {
        case '\n': // Ignore new-lines
          break;
        case '\r': // Return on CR
          rpos = pos;
          pos = 0;  // Reset position index ready for next time
          return rpos;
        default:
          if (pos < len-1) {
            buffer[pos++] = readch;
            buffer[pos] = 0;
          }
      }
    }
    // No end of line has been found, so return -1.
    return -1;
  }

  void loop() override {
    const int max_line_length = 80;
    static char buffer[max_line_length];
    while (available()) {
      if(readline(read(), buffer, max_line_length) > 0) {
        // nacteny radek v bufferu (jeden, bez "\n")
        
        string line(buffer); //convert to string
        received_line->publish_state(line);     // publish line
        vector<string> data = split (line+"   ", ' ');
        //ESP_LOGD("JA121T", "Data: '%s' '%s' '%s'", data[0].c_str(), data[1].c_str(), data[2].c_str());
        //Version
        if (data[0].compare("JA-121T,") == 0) {
          ESP_LOGD("JA121T", "Version: %s", line.c_str());
          version->publish_state(line);
        }  
        //OK
        if (data[0].compare("OK") == 0) {
          ESP_LOGD("JA121T", "> OK");
          error->publish_state("0");
        }  
        //ERROR
        if (data[0].compare("ERROR") == 0) {
          ESP_LOGD("JA121T", "> ERROR");
          error->publish_state("1");
        }
        //PGs
        if (data[0].compare("PG") == 0) {
          //ESP_LOGD("JA121T", "> PG");
          int pg=atoi( data[1].c_str() );
          //ESP_LOGD("JA121T", "pg= %i", pg);      
          int size=(sizeof(PG)/sizeof(*PG));
          //ESP_LOGD("JA121T", "size= %i", size);  
          if (pg<size) {          
            //ESP_LOGD("JA121T", "> - %i", pg); 
            ESP_LOGD("JA121T", "> PG %i is %s", pg, data[2].c_str());
            if (data[2].compare("OFF") == 0) {
              PG[pg]->publish_state("0");
            }  
            if (data[2].compare("ON") == 0) {
              PG[pg]->publish_state("1");
            }
          }
          else
          {
            ESP_LOGD("JA121T", "PG %i out of array", pg); 
          }
        }
        //STATE
        if (data[0].compare("STATE") == 0) {
          int state=atoi( data[1].c_str() );
          //ESP_LOGD("JA121T", "state= %i", state);      
          int size=(sizeof(STATE)/sizeof(*STATE));
          //ESP_LOGD("JA121T", "size= %i", size);  
          if (state<size) {  
            //ESP_LOGD("JA121T", "> - %i", state);             
            ESP_LOGD("JA121T", "> STATE %i is %s",state,data[2].c_str());
            STATE[state]->publish_state(data[2].c_str());
            if (data[2].compare("READY") == 0) {                     
            }
            if (data[2].compare("ARMED") == 0) {
            }  
            if (data[2].compare("ARMED_PART") == 0) {
            }  
            if (data[2].compare("MAINTENANCE") == 0) {
            } 
            if (data[2].compare("SERVICE") == 0) {
            }  
            if (data[2].compare("BLOCKED") == 0) {
            }  
            if (data[2].compare("OFF") == 0) {
            }
          }  
          else
          {
            ESP_LOGD("JA121T", "STATE %i out of array", state); 
          }
        }  
        //PRFSTATE
        if (data[0].compare("PRFSTATE") == 0) {
          for (int i = 0; i < 8; i++) { //8x8=64 perif.
            string one="0x"+data[1].substr (i*2,2); //each char of responce from left
            stringstream ss;
            ss << hex << one;
            unsigned n;
            ss >> n;
            bitset<8> b(n);
            ESP_LOGD("JA121T", "> Decoded byte %i: %s",i,b.to_string().c_str());
            /*
            for (int j = 0; j < 8; j++) {
              if (b[j] >0) {
                ESP_LOGD("JA121T", "> Perif %i = 1",(j+(i*8)));
                PRFSTATE[j+(i*8)]->publish_state("1");
              }
              else{
                PRFSTATE[j+(i*8)]->publish_state("0");
              }  
            }
            */
          }  
        }        
      }
    }
  }
};